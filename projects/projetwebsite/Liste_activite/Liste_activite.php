<!DOCTYPE html>
<html>
<head><title>ESIGELEC Telethon</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" type="text/css" href="side_lst_act.css">
	<style type="text/css">
	#A2:link
	{
		background:#779ffd;
	}
	</style>
</head>
<body>
	<div id="all">
	<div id="header">
		<div id="panneau">
			<img id="logo" src="../Logo_ESIGELEC.PNG" alt="Logo">
			<div id="login">
				<?php
				if (isset($_COOKIE["user_mail"]))
					include '../loggedin.php';
				else
					include '../anonyme.php';
				?>
			</div>
		</div>
		<div id="navigator">
			<ul>
            <li><a id="A1" href="../Index.php" title="">Accueil</a></li>
            <li><a id="A2" href="../Liste_activite/Liste_activite.php" title="">Liste activite</a></li>
            <li><a id="A3" href="../liste_membre/liste_membre.php" title="">Liste des membres</a></li>
            <li><a id="A4" href="../Compte/Compte.php" title="">Espace compte</a></li>
      </ul>
		</div>
	</div>
	<div id="content_back">
		<div id="content">
			<div id="side_lst_act">
				<br/><br/><br/>
					<a href="creer_act.php">Creer une activite</a>
					<br/><br/>
					<a href="../Compte/gerer_act.php">gerer vos activites</a>
			</div>
			<div id="lst_act">
				<?php
				$con = mysql_connect("127.0.0.1:3306","root","");
				if (!$con)
					echo "<script>alert(\"Echec de connexion de base de donnees!\");</script>";
				mysql_select_db("telethon", $con);
				$result = mysql_query("SELECT * FROM activites");
				echo "<table id=\"list\">
				<tr>
				<th>Nom d'activite</th>
				<th>Date</th>
				<th>Lieu</th>
				</tr>";
				
				while($row = mysql_fetch_array($result))
				  {
				  	$url="detail_act.php?act_id=".$row['act_id'];
					  echo "<tr>";
					  echo "<td><a href=\"".$url."\">" . $row['act_nom'] . "</a></td>";
					  echo "<td>" . $row['act_date'] . "</td>";
					  echo "<td>" . $row['act_lieu'] . "</td>";
					  echo "</tr>";
				  }
				echo "</table>";
				mysql_close($con);
				?>
			</div>
		</div>
	</div>
	</div>
</body>
</html>