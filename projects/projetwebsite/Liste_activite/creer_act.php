<!DOCTYPE html>
<html>
<head><title>ESIGELEC Telethon</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" type="text/css" href="side_lst_act.css">
	<style type="text/css">
	#A2:link
	{
		background:#779ffd;
	}
	</style>
</head>
<body>
	<div id="all">
	<div id="header">
		<div id="panneau">
			<img id="logo" src="../Logo_ESIGELEC.PNG" alt="Logo">
			<div id="login">
				<?php
				if (isset($_COOKIE["user_mail"]))
					include '../loggedin.php';
				else
					include '../anonyme.php';
				?>
			</div>
		</div>
		<div id="navigator">
			<ul>
            <li><a id="A1" href="../Index.php" title="">Accueil</a></li>
            <li><a id="A2" href="../Liste_activite/Liste_activite.php" title="">Liste activite</a></li>
            <li><a id="A3" href="../liste_membre/liste_membre.php" title="">Liste des membres</a></li>
            <li><a id="A4" href="../Compte/Compte.php" title="">Espace compte</a></li>
      </ul>
		</div>
	</div>
	<div id="content_back">
		<div id="content">
			<div id="side_lst_act">
				<br/><br/><br/>
					<a href="creer_act.php">Creer une activite</a>
					<br/><br/>
					<a href="../Compte/gerer_act.php">gerer vos activites</a>
			</div>
			<div id="lst_act">
				<form action="creer_act_fini.php" method="POST">
					<table style="width:700px">
						<tbody>
							<tr>
								<th style="width: 150px;text-align: right;">Nom:</th>
								<th><input type="text" name="nom"/></th>
							</tr>
							<tr>
								<th style="text-align:right;">Date:</th>
								<th><input type="text" name="date"/></th>
							</tr>
							<tr>
								<th style="text-align:right;">Lieu:</th>
								<th><input type="text" name="lieu"/></th>
							</tr>
							<tr>
								<th style="text-align:right;">Heure:</th>
								<th><input type="text" name="heure"/></th>
							</tr>
							<tr>
								<th style="text-align:right;">Description:</th>
								<th><textarea rows="10" cols="30" name="description"></textarea></th>
							</tr>
						</tbody>
					</table>
					<input type="submit" value="Submit" style="position:relative;left:150px;"/>
				</form>
			</div>
		</div>
	</div>
	</div>
</body>
</html>