<!DOCTYPE html>
<html>
<head><title>ESIGELEC Telethon</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" type="text/css" href="../liste_membre/liste_membre.css">
	<style type="text/css">
	#A4:link
	{
		background:#779ffd;
	}
	</style>
</head>
<body>
	<div id="all">
	<div id="header">
		<div id="panneau">
			<img id="logo" src="../Logo_ESIGELEC.PNG" alt="Logo">
			<div id="login">
				<?php
				if (isset($_COOKIE["user_mail"]))
					include '../loggedin.php';
				else
					include '../anonyme.php';
				?>
			</div>
		</div>
		<div id="navigator">
			<ul>
            <li><a id="A1" href="../Index.php" title="">Accueil</a></li>
            <li><a id="A2" href="../Liste_activite/Liste_activite.php" title="">Liste activite</a></li>
            <li><a id="A3" href="../liste_membre/liste_membre.php" title="">Liste des membres</a></li>
            <li><a id="A4" href="../Compte/Compte.php" title="">Espace compte</a></li>
      </ul>
		</div>
	</div>
	<div id="content_back">
		<div id="content">
			<div id="side_lst_act">
				<br/><br/><br/>
					<a href="Compte.php">gerer votre compte</a>
					<br/><br/>
					<a href="../Liste_activite/creer_act.php">Creer une activite</a>
					<br/><br/>
					<a href="../Compte/gerer_act.php">gerer vos activites</a>
			</div>
			<div id="lst_act">
				<?php
				if(!isset($_COOKIE['user_id']))
				{
					echo '<script>';
					echo 'alert("Veuillez connecter!");';
					echo '</script>';
					$url = "../Index.php";
					echo "<script language='javascript' type='text/javascript'>";
					echo "window.location.href='$url';";
					echo "</script>";
				}
				$con = mysql_connect("127.0.0.1:3306","root","");
				if (!$con)
					echo "<script>alert(\"Echec de connexion de base de donnees!\");</script>";
				mysql_select_db("telethon", $con);
				$command="select* from activites where act_president=".$_COOKIE['user_id'];
				$result = mysql_query($command);
				echo "<p>Les activites cree par vous";
				echo "<table id=\"list\" style=\"width:100%;\">
				<tr>
				<th></th>
				<th>Nom</th>
				<th>Date</th>
				<th>Lieu</th>
				<th>Heure</th>
				<th>Inscrits</th>
				<th></th>
				</tr>";
				
				while($row = mysql_fetch_array($result))
				  {
				  	$url="../Liste_activite/detail_act.php?act_id=".$row['act_id'];
				  	$url1="list_ins.php?act_id=".$row['act_id'];
				  	$url2="modifier_act.php?act_id=".$row['act_id'];
					  echo "<tr>";
					  echo "<th><a href=\"".$url."\">-></a></th>";
					  echo "<th>" . $row['act_nom'] . "</th>";
					  echo "<th>" . $row['act_date'] . "</th>";
					  echo "<th>" . $row['act_lieu'] . "</th>";
					  echo "<th>" . $row['act_heure'] . "</th>";
					  echo "<th><a href=\"".$url1."\">consulter</a></th>";
					  echo "<th><a href=\"".$url2."\">modifier</a></th>";
					  echo "</tr>";
				  }
				echo "</table>";
				mysql_close($con);
				?>
			</div>
		</div>
	</div>
	</div>
</body>
</html>